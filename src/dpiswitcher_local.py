#!/usr/bin/env python
'''
This extension scales a document to fit different SVG DPI -90/96-

Copyright (C) 2012 Jabiertxo Arraiza, jabier.arraiza@marker.es
Copyright (C) 2016 su_v, <suv-sf@users.sf.net>

Version 0.6 - DPI Switcher

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


Changes since v0.5:
    - transform all top-level containers and graphics elements
    - support scientific notation in SVG lengths
    - fix scaling with existing matrix() transformation
      (use functions from simpletransform.py)
    - support different units for document width, height attributes
    - improve viewBox support (syntax, offset)
    - support common cases of text-put-on-path in SVG root
    - support common cases of <use> references in SVG root
    - examples from http://tavmjong.free.fr/INKSCAPE/UNITS/ tested

TODO:
    - check grids/guides created with 0.91:
      http://tavmjong.free.fr/INKSCAPE/UNITS/units_mm_nv_90dpi.svg
    - check <symbol> instances
    - check more <use> and text-on-path cases (reverse scaling needed?)
    - scale perspective of 3dboxes

FIXME:
    - instances and referenced elements
    If for example a referenced element in SVG root is directly scaled,
    and its instance (referencing element e.g. <use>) is inside a scaled
    top-level container, the instance in the end will be rendered at an
    incorrect scale relative to the viewport (page area) and the other
    drawing content. Another unsupported case is both the referenced
    element and the instance in SVG root: the clone will in the end be
    rendered with the scale factor applied twice.

'''
# standard libraries
import re
import math
import time
from fractions import Fraction
# local libraries
import inkex
import simpletransform
import simplestyle


# globals
SKIP_CONTAINERS = [
    'defs',
    'glyph',
    'marker',
    'mask',
    'missing-glyph',
    'pattern',
    'symbol',
]
CONTAINER_ELEMENTS = [
    'a',
    'g',
    'switch',
]
GRAPHICS_ELEMENTS = [
    'circle',
    'ellipse',
    'image',
    'line',
    'path',
    'polygon',
    'polyline',
    'rect',
    'text',
    'use',
]


# ----- helper functions for inkscape version

def str_to_int(val):
    """Return first int in string."""
    if isinstance(val, str):
        split_val = re.match(r'(\d*)(.*$)', val).groups()
        if len(split_val):
            return int(split_val[0])
        else:
            return 0


def version_from_string(version_str):
    """Read version from string ('.' as spearator)."""
    dot_split = version_str.strip().split('.')
    version = []
    for substring in dot_split:
        version.append(str_to_int(substring))
    return version


def version_inside_range(version, major_min, minor_min, major_max, minor_max):
    """Compare version to major and minor range of versions."""
    if version[0] < major_min or version[0] > major_max:
        return False
    elif version[0] == major_min and version[1] <= minor_min:
        return False
    elif version[0] == major_max and version[1] >= minor_max:
        return False
    else:
        return True


def dpi_timestamp(node):
    """Add desc element with datetime string to node."""
    timestamp = time.strftime('%Y-%m-%d %H:%M:%S')
    desc_id = 'dpi_timestamp'
    path = '//svg:desc[@id="{}"]'.format(desc_id)
    desc_list = node.xpath(path, namespaces=inkex.NSS)
    if isinstance(desc_list, list) and len(desc_list):
        element = desc_list[0]
    else:
        element = inkex.etree.Element(inkex.addNS('desc', 'svg'))
        element.set('id', desc_id)
        node.insert(0, element)
    element.text = timestamp


# ----- helper functions for transforms

def format_scale(fx, fy):
    """Format scale() transformation with fractions."""
    return 'scale({},{})'.format(float(fx), float(fy))


def normalize_pt2(point):
    """Normalize coordinates of projected 3d point."""
    if math.fabs(point[2]) < 1E-6 or point[2] == 1.0:
        return point
    point[0] /= point[2]
    point[1] /= point[2]
    point[2] = 1.0
    return point


# ----- helper functions for processing SVG elements

def is_perspective(element):
    """Check whether element is an Inkscape 3dbox perspective."""
    return (element.tag == inkex.addNS('perspective', 'inkscape') and
            element.get(inkex.addNS('type', 'sodipodi')) == 'inkscape:persp3d')


def is_3dbox(element):
    """Check whether element is an Inkscape 3dbox type."""
    return (element.tag == inkex.addNS('g', 'svg') and
            element.get(inkex.addNS('type', 'sodipodi')) == 'inkscape:box3d')


def is_3dbox_side(element):
    """Check if element is box3dsidetype."""
    return (element.tag == inkex.addNS('path', 'svg') and
            element.get(
                inkex.addNS('type', 'sodipodi')) == 'inkscape:box3dside')


def is_use(element):
    """Check whether element is of type <text>."""
    return element.tag == inkex.addNS('use', 'svg')


def is_text(element):
    """Check whether element is of type <text>."""
    return element.tag == inkex.addNS('text', 'svg')


def is_text_on_path(element):
    """Check whether text element is put on a path."""
    if is_text(element):
        text_path = element.find(inkex.addNS('textPath', 'svg'))
        if text_path is not None:
            return True
    return False


def is_sibling(element1, element2):
    """Check whether element1 and element2 are siblings of same parent."""
    return element2 in element1.getparent()


def is_descendant_of_sibling(element1, element2):
    """Check whether element2 is a descendant of element1 siblings."""
    for sibling in element1.itersiblings(preceding=True):
        if element2 in sibling.iterdescendants():
            return True
    for sibling in element1.itersiblings(preceding=False):
        if element2 in sibling.iterdescendants():
            return True
    return False


def is_in_defs(doc, element):
    """Check whether element is in defs."""
    if element is not None:
        defs = doc.find('defs', namespaces=inkex.NSS)
        if defs is not None:
            return element in defs.iterdescendants()
    return False


def is_linked_to_defs(doc, element):
    """Check whether linked element is hidden in <defs> element."""
    if element is not None:
        linked_node = get_linked(doc, element)
        defs = doc.find('defs', namespaces=inkex.NSS)
        if linked_node is not None and defs is not None:
            return linked_node in defs.iterdescendants()
    return False


def get_linked(doc, element):
    """Return linked element or None."""
    if element is not None:
        href = element.get(inkex.addNS('href', 'xlink'), None)
    if href is not None:
        linked_id = href[href.find('#')+1:]
        path = '//*[@id="%s"]' % linked_id
        el_list = doc.xpath(path, namespaces=inkex.NSS)
        if isinstance(el_list, list) and len(el_list):
            return el_list[0]
        else:
            return None


# ----- helper functions for SVG lengths and coordinates

def parse_length(val):
    """Parse SVG length into list of float (value) and string (unit)."""
    if isinstance(val, str):
        # TODO: support scientifc notation
        split_val = re.match(r'([+-]?\d*\.?\d*)(.*$)', val).groups()
        return [float(split_val[0]), split_val[1]]


def print_length(alist):
    """Return length string."""
    return '{}{}'.format(*alist)


def parse_point(val):
    """Parse point string into list of floats."""
    # pylint: disable=bad-builtin
    return [float(v) for v in filter(None, re.split("[,]", val))]


def print_point(alist):
    """Return point list as string joined with comma."""
    return ' '.join(str(v) for v in alist)


def parse_list_of_lengths(val):
    """Parse list-of-lengths string into list of lengths."""
    # pylint: disable=bad-builtin
    return [parse_length(v) for v in filter(None, re.split("[, ]+", val))]


def print_list_of_lengths(alist):
    """Return list-of-lengths list as string joined with whitespace."""
    return ' '.join(print_length(l) for l in alist)


def parse_list_of_numbers(val):
    # pylint: disable=bad-builtin
    """Parse list-of-numbers string into list of floats."""
    return [float(v) for v in filter(None, re.split("[, ]+", val))]


def print_list_of_numbers(alist):
    """Return list-of-numbers list as string joined with whitespace."""
    return ' '.join(str(v) for v in alist)


def parse_list_of_points(val):
    """Parse list-of-points string into list of floats."""
    # pylint: disable=bad-builtin
    # FIXME: regex for split
    return [parse_point(v) for v in filter(None, re.split("[ ]+", val))]


def print_list_of_points(alist):
    """Return points list as string joined with whitespace."""
    return ' '.join(print_point(v) for v in alist)


def parse_list_of_3dcoords(val):
    """Parse 3dpoint string into list of floats."""
    # pylint: disable=bad-builtin
    return [float(v) for v in filter(None, re.split("[:]", val))]


def print_list_of_3dcoords(alist):
    """Return 3dpoint list as string joined with whitespace."""
    return ' : '.join(str(v) for v in alist)


def scale_list_of_lengths(element, prop, scale):
    """Scale list of lengths for text position shifts."""
    if prop in element.attrib:
        lengths = parse_list_of_lengths(element.get(prop))
        for length in lengths:
            length[0] *= scale
        element.set(prop, print_list_of_lengths(lengths))


def scale_props_length(element, props, scale):
    """Scale length in list of properties of element."""
    sdict = simplestyle.parseStyle(element.get('style'))
    for prop in props:
        if prop in element.attrib:
            val, unit = parse_length(element.get(prop))
            element.set(prop, print_length([val * scale, unit]))
        if prop in sdict:
            val, unit = parse_length(sdict[prop])
            sdict[prop] = print_length([val * scale, unit])
            element.set('style', simplestyle.formatStyle(sdict))


# ----- scale attributes of elements

def scale_persp3d_attribs(element, scale_x, scale_y):
    """Scale attributes of perspective definition."""
    props = ['persp3d-origin', 'vp_x', 'vp_y', 'vp_z']
    for prop in [inkex.addNS(v, 'inkscape') for v in props]:
        if prop in element.attrib:
            pt2 = normalize_pt2(parse_list_of_3dcoords(element.get(prop)))
            pt2[0] *= scale_x
            pt2[1] *= scale_y
            element.set(prop, print_list_of_3dcoords(pt2))


def scale_text_attribs(element, scale_x, scale_y):
    """Scale individual font attributes and properties."""
    mat = simpletransform.parseTransform(format_scale(scale_x, scale_y))
    det = mat[0][0]*mat[1][1] - mat[0][1]*mat[1][0]
    descrim = math.sqrt(abs(det))
    props = ['font-size', 'letter-spacing', 'word-spacing', 'stroke-width']
    # outer text
    scale_list_of_lengths(element, 'dx', scale_x)
    scale_list_of_lengths(element, 'dy', scale_y)
    scale_props_length(element, props, descrim)
    # inner tspans
    for child in element.iterdescendants():
        if child.tag == inkex.addNS('tspan', 'svg'):
            scale_list_of_lengths(child, 'dx', scale_x)
            scale_list_of_lengths(child, 'dy', scale_y)
            scale_props_length(child, props, descrim)


def scale_use_attribs(element, scale_x, scale_y):
    """Scale individual use attributes and properties."""
    mat = simpletransform.parseTransform(format_scale(scale_x, scale_y))
    det = mat[0][0]*mat[1][1] - mat[0][1]*mat[1][0]
    descrim = math.sqrt(abs(det))
    props = ['stroke-width']
    # use
    scale_props_length(element, props, descrim)


# ----- check special cases

def check_defs(svg, element, scale_x, scale_y):
    """Check defs for specific elements to scale."""
    # pylint: disable=unused-argument
    skip = True
    for child in element:
        if is_perspective(child):
            scale_persp3d_attribs(child, scale_x, scale_y)
    return skip


def check_3dbox(svg, element, scale_x, scale_y):
    """Check transformation for 3dbox element."""
    # pylint: disable=unused-argument
    skip = False  # don't skip: transform will scale style properties
    return skip


def check_text_on_path(svg, element, scale_x, scale_y):
    """Check whether to skip scaling a text put on a path."""
    skip = False
    path = get_linked(svg, element.find(inkex.addNS('textPath', 'svg')))
    if not is_in_defs(svg, path):
        if is_sibling(element, path):
            # skip common element scaling if both text and path are siblings
            skip = True
            # scale offset
            if 'transform' in element.attrib:
                mat = simpletransform.parseTransform(element.get('transform'))
                mat[0][2] *= scale_x
                mat[1][2] *= scale_y
                element.set('transform', simpletransform.formatTransform(mat))
            # scale font properties
            scale_text_attribs(element, scale_x, scale_y)
    return skip


def check_use(svg, element, scale_x, scale_y):
    """Check whether to skip scaling an instanciated element (<use>)."""
    skip = False
    path = get_linked(svg, element)
    if not is_in_defs(svg, path):
        if is_sibling(element, path):
            skip = True
            # scale offset
            if 'transform' in element.attrib:
                mat = simpletransform.parseTransform(element.get('transform'))
                mat[0][2] *= scale_x
                mat[1][2] *= scale_y
                element.set('transform', simpletransform.formatTransform(mat))
            # scale lengths in style properties
            scale_use_attribs(element, scale_x, scale_y)
    return skip


class DPISwitcher(inkex.Effect):
    """inkex.Effect()-based class to switch internal dpi of SVG document."""

    def __init__(self):
        """Init parent class and DPISwitcher() options."""
        inkex.Effect.__init__(self)

        # instance attributes
        self.verbose = False
        self.inkversion = [0, 0]
        self.inkruntime = [0, 0]
        self.legacy = False
        self.viewboxed = False
        self.plain_svg = False

        # options
        self.OptionParser.add_option("--switcher",
                                     action="store", type="string",
                                     dest="switcher", default="dpi90to96",
                                     help="Select the DPI switch you want")
        self.OptionParser.add_option("--fix_guides",
                                     action="store", type="inkbool",
                                     dest="fix_guides", default=False,
                                     help="Fix guides")
        self.OptionParser.add_option("--fix_grids",
                                     action="store", type="inkbool",
                                     dest="fix_grids", default=False,
                                     help="Fix grids")
        self.OptionParser.add_option("--verbose",
                                     action="store", type="inkbool",
                                     dest="verbose", default=False,
                                     help="Verbose")
        # notebooks
        self.OptionParser.add_option("--action",
                                     action="store", type="string",
                                     dest="action", default=None,
                                     help="Name of active UI tab")

    # dictionaries of unit to user unit conversion factors
    __uuconv_90dpi = {
        'in': 90.0,
        'pt': 1.25,
        'px': 1.0,
        'mm': 3.5433070866,
        'cm': 35.433070866,
        'm': 3543.3070866,
        'km': 3543307.0866,
        'pc': 15.0,
        'yd': 3240.0,
        'ft': 1080.0,
    }
    __uuconv_96dpi = {
        'in': 96.0,
        'pt': 1.33333333333,
        'px': 1.0,
        'mm': 3.77952755913,
        'cm': 37.7952755913,
        'm': 3779.52755913,
        'km': 3779527.55913,
        'pc': 16.0,
        'yd': 3456.0,
        'ft': 1152.0,
    }

    def showme(self, msg=''):
        """Wrapper for debug output."""
        if self.verbose:
            inkex.debug(msg)

    # ----- version checks

    def inkscape_version_runtime(self):
        """Check runtime inkscape version information."""
        # TODO: implement function to access runtime inkscape version via cli.
        # It will be based on the assumption that the running process is the
        # first 'inkscape' executable in $PATH (not save!).
        #
        # For now, rely on inkex features and pixels per inch (dpi) to
        # determine runtime inkscape version:
        try:
            px_per_in = self.uutounit(self.unittouu('1in'), 'px')
            if px_per_in == 90.0:
                self.inkruntime[1] = 91
            elif px_per_in == 96.0:
                self.inkruntime[1] = 92
        except AttributeError:
            # pylint: disable=no-member
            px_per_in = inkex.uutounit(inkex.unittouu('1in'), 'px')
            self.inkruntime[1] = 48
        # Verbose output
        runtime = '{}.{}'.format(*self.inkruntime)
        self.showme(
            'Assuming runtime is {} ({}dpi)\n'.format(runtime, int(px_per_in)))

    def inkscape_version_doc(self, svg):
        """Check document's inkscape version information."""
        inkversion_attr = inkex.addNS('version', 'inkscape')
        if inkversion_attr in svg.attrib:
            version = version_from_string(svg.get(inkversion_attr))
            legacy048 = version_inside_range(version, 0, 1, 0, 91)
            legacy091 = version_inside_range(version, 0, 1, 0, 92)
            self.inkversion = version[:2]
            self.legacy = legacy048 or legacy091
        else:
            self.plain_svg = True  # needs better check (namespace)

    # ----- helper methods

    def parse_length(self, length, percent=False, pixel=True):
        """Parse SVG length."""
        val = 100.0                         # fallback
        unit = '' if not pixel else 'px'    # fallback
        if length:
            if self.options.switcher == 'dpi90to96':
                known_units = self.__uuconv_90dpi.keys()
            elif self.options.switcher == 'dpi96to90':
                known_units = self.__uuconv_96dpi.keys()
            else:
                known_units = ['px']
            if percent:
                unitmatch = re.compile('(%s)$' % '|'.join(known_units + ['%']))
            else:
                unitmatch = re.compile('(%s)$' % '|'.join(known_units))
            param = re.compile(
                r'(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)')
            p = param.match(length)
            u = unitmatch.search(length)
            if p:
                val = float(p.string[p.start():p.end()])
            if u:
                unit = u.string[u.start():u.end()]
        return (val, unit)

    def convert_length(self, val, unit=None, to_unit=None):
        """Convert length to *to_unit* if unit differs."""
        if unit is None:
            unit = 'px'
        if to_unit is None:
            to_unit = 'px'
        if unit != to_unit:
            if self.options.switcher == 'dpi90to96':
                val_px = val * self.__uuconv_90dpi[unit]
                val = (val_px / (self.__uuconv_90dpi[to_unit] /
                                 self.__uuconv_90dpi['px']))
                unit = to_unit
            elif self.options.switcher == 'dpi96to90':
                val_px = val * self.__uuconv_96dpi[unit]
                val = (val_px / (self.__uuconv_96dpi[to_unit] /
                                 self.__uuconv_96dpi['px']))
                unit = to_unit
            else:
                pass
        return (val, unit)

    def check_attr_unit(self, element, attr, unit_list):
        """Check unit of attribute value, match to units in *unit_list*."""
        if attr in element.attrib:
            unit = self.parse_length(element.get(attr), percent=True)[1]
            return unit in unit_list

    def scale_attr_val(self, element, attr, unit_list, factor):
        """Scale attribute value if unit matches one in *unit_list*."""
        if attr in element.attrib:
            val, unit = self.parse_length(element.get(attr), percent=True)
            if unit in unit_list:
                element.set(attr, '{}{}'.format(val * factor, unit))

    def scale_guide_attr(self, svg, guide, attr, dpifactor, scale_x, scale_y):
        """Scale guide attribute."""
        # pylint: disable=too-many-arguments
        # get guide attribute
        self.showme('old {}: {}'.format(attr, guide.get(attr)))
        point = parse_point(guide.get(attr, ''))
        if len(point) <= 1:
            # TODO: support legacy guide format?
            self.showme('Unsupported legacy guide format, not scaled.')
            return
        # scale coordinates
        if self.viewboxed:  # document with viewBox
            self.showme('viewBox present on load.')
            self.showme('Guide in user units.')
            point[0] *= scale_x * dpifactor
            point[1] *= scale_y * dpifactor
        else:  # legacy document without viewBox
            self.showme('no viewBox on load.')
            if 'viewBox' not in svg.attrib:
                self.showme('no viewBox added either.')
                # viewport in px units, no viewBox added
                # guide position in CSS px, scale by dpi factor
                self.showme('Legacy guide in CSS px.')
                point[0] *= dpifactor
                point[1] *= dpifactor
            else:
                self.showme('viewBox added by extension.')
                # viewport in absolute units, viewBox added
                # guide position in user units
                if self.inkruntime[1] > 91:
                    # 0.92: guides are scaled internally
                    self.showme(
                        'Scaling of guides done internally.')
                elif self.inkruntime[1] == 91:
                    # 0.91: guides need scaling to user units
                    point[0] *= scale_x * dpifactor
                    point[1] *= scale_y * dpifactor
                    self.showme(
                        'Reload the document to update guides.')
                else:
                    # 0.48: inkscape only supports guides in CSS px
                    point[0] *= scale_x * dpifactor
                    point[1] *= scale_y * dpifactor
                    self.showme(
                        'Scaled guides do not display correctly ' +
                        'in this version of Inkscape. ' +
                        'Save and open the scaled document ' +
                        'in Inkscape 0.91 or later.')
        # update guide attribute
        guide.set(attr, '{},{}'.format(*point))
        self.showme('new {}: {}'.format(attr, guide.get(attr)))

    def scale_axonomgrid_attribs(self, svg, grid, dpifactor, scale_x, scale_y):
        """Scale attributes of xygrid."""
        # pylint: disable=too-many-arguments,unused-argument
        # TODO: support scaling of axonometric grids
        pass

    def scale_xygrid_attribs(self, svg, grid, dpifactor, scale_x, scale_y):
        """Scale attributes of xygrid."""
        # pylint: disable=too-many-arguments,unused-argument
        attribs = {'originx': "0.0", 'originy': "0.0",
                   'spacingx': "1.0", 'spacingy': "1.0"}
        # get grid attribute
        for attr in attribs.keys():
            if attr in grid.attrib:
                self.showme('old {}: {}'.format(attr, grid.get(attr)))
                val, unit = self.parse_length(grid.get(attr), pixel=False)
            else:
                val, unit = self.parse_length(attribs[attr], pixel=False)
            self.showme([val, unit])
            # scale length
            if unit:
                grid_unit = grid.get('units')
                self.showme('Legacy grid: {}{}, unit:{}'.format(
                    val, unit, grid_unit))
                if unit == 'px':
                    # grid length in CSS px, scale with dpi factor
                    val *= dpifactor
                else:
                    # absolute length, no scaling needed
                    pass
            else:
                # grid length in user units
                if attr.endswith('x'):
                    val *= scale_x * dpifactor
                elif attr.endswith('y'):
                    val *= scale_y * dpifactor
            # update grid attribute
            grid.set(attr, '{}{}'.format(val, unit))
            self.showme('new {}: {}'.format(attr, grid.get(attr)))

    # ----- scale elements of drawing content

    def scale_guides(self, svg, dpifactor, scale_x, scale_y):
        """Scale inkscape guide lines."""
        xpath_str = '//sodipodi:guide'
        guides = svg.xpath(xpath_str, namespaces=inkex.NSS)
        attribs = ['position']
        for guide in guides:
            self.showme('\nguide id: {}'.format(guide.get('id')))
            for attr in attribs:
                if attr in guide.attrib:
                    self.scale_guide_attr(
                        svg, guide, attr, dpifactor, scale_x, scale_y)

    def scale_grids(self, svg, dpifactor, scale_x, scale_y):
        """Scale inkscape grids."""
        # TODO: support default grids (values from prefs?)
        xpath_str = '//inkscape:grid'
        grids = svg.xpath(xpath_str, namespaces=inkex.NSS)
        for grid in grids:
            self.showme('\ngrid id: {}'.format(grid.get('id')))
            if 'type' in grid.attrib:
                if grid.get('type') == 'xygrid':
                    self.scale_xygrid_attribs(
                        svg, grid, dpifactor, scale_x, scale_y)
                elif grid.get('type') == 'axonomgrid':
                    self.scale_axonomgrid_attribs(
                        svg, grid, dpifactor, scale_x, scale_y)

    def scale_elements(self, svg, scale_x, scale_y):
        """Scale top-level elements in XML node *svg*."""
        # pylint: disable=too-many-branches
        for element in svg:

            if element.tag is inkex.etree.Comment:
                continue

            tag = inkex.etree.QName(element).localname

            if tag == 'defs':
                # scale specific elements in defs
                if check_defs(svg, element, scale_x, scale_y):
                    continue

            if tag in GRAPHICS_ELEMENTS or tag in CONTAINER_ELEMENTS:
                # test for specific elements to skip from scaling
                if is_3dbox(element):
                    if check_3dbox(svg, element, scale_x, scale_y):
                        continue
                if is_text_on_path(element):
                    if check_text_on_path(svg, element, scale_x, scale_y):
                        continue
                if is_use(element):
                    if check_use(svg, element, scale_x, scale_y):
                        continue
                # relative units ('%') in presentation attributes
                for attr in ['width', 'x']:
                    self.scale_attr_val(
                        element, attr, ['%'], 1.0/scale_x)
                for attr in ['height', 'y']:
                    self.scale_attr_val(
                        element, attr, ['%'], 1.0/scale_y)
                # set preserved transforms on top-level elements
                if scale_x != 1.0 and scale_y != 1.0:
                    mat = simpletransform.parseTransform(
                        format_scale(scale_x, scale_y))
                    simpletransform.applyTransformToNode(mat, element)

    def scale_root(self, svg, dpifactor):
        """Scale SVG viewport and top-level elements in SVG root."""
        vblist = []
        w_unit = h_unit = 'px'
        width = height = 100
        # current viewBox
        if 'viewBox' in svg.attrib:
            vblist = parse_list_of_numbers(svg.get('viewBox'))
        # viewport size
        if 'width' in svg.attrib:
            width, w_unit = self.parse_length(svg.get('width'))
        elif len(vblist):
            width = vblist[2]
        if 'height' in svg.attrib:
            height, h_unit = self.parse_length(svg.get('height'))
        elif len(vblist):
            height = vblist[3]
        # computed lengths in CSS px
        width_px = self.convert_length(width, w_unit, to_unit='px')[0]
        height_px = self.convert_length(height, h_unit, to_unit='px')[0]
        # computed viewBox
        if 'viewBox' not in svg.attrib:
            vblist = [0, 0, width_px, height_px]
        # document scale
        old_scale = [width_px / vblist[2], height_px / vblist[3]]
        new_scale = [(i * dpifactor) for i in old_scale]
        # new viewBox
        vblist_new = [vblist[0] * new_scale[0], vblist[1] * new_scale[1],
                      vblist[2] * new_scale[0], vblist[3] * new_scale[1]]
        # update viewport if in px
        if w_unit in ['', 'px']:
            svg.set('width', print_length([width * dpifactor, w_unit]))
        if h_unit in ['', 'px']:
            svg.set('height', print_length([height * dpifactor, h_unit]))
        # update viewBox
        if ('viewBox' in svg.attrib or
                (w_unit not in ['', 'px'] or h_unit not in ['', 'px'])):
            svg.set('viewBox', print_list_of_numbers(vblist_new))

        self.showme('old viewBox: {}'.format(vblist))
        self.showme('old scale: {}'.format(old_scale))
        self.showme('dpifactor: {}'.format(float(dpifactor)))
        self.showme('new viewBox: {}'.format(vblist_new))
        self.showme('new scale: {}'.format(new_scale))

        # update guides, grids
        if self.options.fix_guides:
            self.scale_guides(svg, dpifactor, *old_scale)
        if self.options.fix_grids:
            self.scale_grids(svg, dpifactor, *old_scale)
        # update top-level elements
        self.scale_elements(svg, *new_scale)

    # ----- basic modes of extension

    def doc_info(self, svg):
        """Display information about current document."""
        # init variables
        self.verbose = True
        show = self.showme

        show("::: SVG document information :::\n")

        if not self.plain_svg:
            show('Last edited with Inkscape {}.{}'.format(*self.inkversion))
            show('Is legacy file: {}'.format(self.legacy))
        else:
            show('Is plain SVG: {}'.format(self.plain_svg))
        show('Is viewboxed: {}'.format(self.viewboxed))
        show('')

        # Inkscape version
        inkversion = inkex.addNS('version', 'inkscape')
        if inkversion in svg.attrib:
            show('inkscape:version: {}'.format(svg.get(inkversion)))
        # SVG root attributes
        for attr in ['width', 'height', 'viewBox']:
            if attr in svg.attrib:
                show('{}: {}'.format(attr, svg.get(attr)))

        show("\n::: Inkscape document properties :::\n")

        # document properties
        nv = svg.find(inkex.addNS('namedview', 'sodipodi'))
        docunits = inkex.addNS('document-units', 'inkscape')
        if docunits in nv.attrib:
            show('inkscape:document-units: {}'.format(nv.get(docunits)))
        if 'units' in nv.attrib:
            show('units: {}'.format(nv.get('units')))
        # guides
        xpath_str = '//sodipodi:guide'
        guides = svg.xpath(xpath_str, namespaces=inkex.NSS)
        if guides:
            show('Document has {} guides'.format(len(guides)))
        # grids
        xpath_str = '//inkscape:grid'
        grids = svg.xpath(xpath_str, namespaces=inkex.NSS)
        for i, grid in enumerate(grids):
            # TODO: detect default grid (uses current prefs, not 'scalable')
            show('Grid number {}: Units: {}'.format(i, grid.get('units')))

    def doc_switch(self, svg):
        """Switch document's internal resolution."""
        switcher = self.options.switcher
        # init dpi scale factor
        if switcher.endswith('90to96'):
            factor = Fraction(96, 90)
        elif switcher.endswith('96to90'):
            factor = Fraction(90, 96)
        else:
            factor = Fraction(1, 1)
        # add timestamp (date, time of dpi switch)
        dpi_timestamp(svg)
        # process elements in SVG root
        self.scale_root(svg, factor)

    def doc_reload(self, svg):
        """Workaround to force a reload of the document."""
        # pylint: disable=no-self-use
        dpi_timestamp(svg)

    # ----- main

    def effect(self):
        """Main routine to modify current document's internal dpi."""
        # init variables
        self.verbose = self.options.verbose
        svg = self.document.getroot()
        # version information (runtime, document)
        self.inkscape_version_runtime()
        self.inkscape_version_doc(svg)
        # initial viewBox
        self.viewboxed = 'viewBox' in svg.attrib
        # switcher action
        if self.options.switcher == 'docinfo':
            self.doc_info(svg)
        elif self.options.switcher.startswith('dpi'):
            self.doc_switch(svg)
        elif self.options.switcher == 'force_reload':
            self.doc_reload(svg)


if __name__ == '__main__':
    ME = DPISwitcher()
    ME.affect()

# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
