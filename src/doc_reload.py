#!/usr/bin/env python
"""
doc_reload.py - Force reload of current document to refresh
                guides, grids and 3D boxes

Copyright (C) 2016 su_v <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# local library
import inkex


class ForceReload(inkex.Effect):

    def __init__(self):
        inkex.Effect.__init__(self)
        self.forcereload = False

    def output(self):
        """Serialize document into XML on stdout"""
        if self.forcereload:
            self.document.write(inkex.sys.stdout)
        else:
            original = inkex.etree.tostring(self.original_document)        
            result = inkex.etree.tostring(self.document)        
            if original != result:
                self.document.write(inkex.sys.stdout)

    def effect(self):
        """Override output() method from inkex.Effect to force reload."""
        self.forcereload = True
 

if __name__ == '__main__':
    ME = ForceReload()
    ME.affect()


# vim: et shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=79
